# pyminer_extensions_store

## 介绍
pyminer的插件商店

## 插件商店文档



所有接口返回json,所有POST接口接受表单参数(Form Data),Get接口接受url参数(Params)

所有接口,如果错误将返回一个形如

```json
{
    "code":"xxx",
    "detail":"xxx"
}
```

的结构,如果没有特殊返回值(从服务器获取的信息),成功时会返回

```json
{
    "code":200,
    "detail":"success"
}
```

PS:已经改造,json中不再出现code,改为http状态码



### POST /api/signin

功能:登录

参数:

username:str

password:str

### POST /api/signup

功能:注册

参数:

username:str

password:str

### Get /api/signout

功能:退出登录

参数:无

### Get /api/user_info

功能:获取当前用户信息

参数:无

返回:

```json
{
    "name":"用户名",
    "id":"用户id(int)"
}
```

### Get /api/extensions

功能:获取插件列表(所有)

ps:之后增加分页功能吧

参数:无

返回:

```json
[
    {
        "id":"插件id",
        "name":"插件名称(唯一名称,英文数字下划线)",
        "display_name":"显示名称",
        "author":"作者的用户名",
        "price":"插件价格(现在全0)"
    }
    ...
]
```

### Get /api/extensions/my

功能:获取所有我购买的插件

ps:之后增加分页功能

参数:无

返回:

````json
[
    {
        "id":"插件id",
        "name":"插件名称(唯一名称,英文数字下划线)",
        "display_name":"显示名称",
        "author":"作者的用户名"
    }
    ...
]
````

### Get /api/extension

功能:获取插件信息

PS:考虑之后增加description,url等

参数​ :

id:插件id

返回:

```json
{
    "id":"插件id",
    "name":"插件名称(唯一名称,英文数字下划线)",
    "display_name":"显示名称",
    "author":"作者的用户名",
    "price":"插件价格(现在全0)"
}
```

### POST /api/extension

功能:新建插件(目前插件里面包含多个插件文件,因为一个插件可能有多个版本,多个平台)

参数:

name:插件名称

display_name:插件显示名称

price:插件价格(目前你给多少都自动设成0)

返回

- 成功时:标准成功返回
- 未登录:code=403,detail=no sign in

-   当名称不唯一:code=409,detail="name must be unique"
-   当缺少参数:code=400,detail="args wrong …"

### POST /api/file

功能:为指定的插件上传一个新版本 

参数:

extension_id:插件id

version:插件版本

file:上传的文件

返回:

-   缺少参数:code=400,detail=params wrong
-   未登录:code=403,detail=no sign in
-   插件id错误:code=400,detail=extension_id_wrong
-   成功:标准成功返回

### POST /api/buy

功能:购买插件(目前价格全0

参数:

extension_id:插件id

返回:

	- 未登录:code=403,detail=no sign in
	- 缺少参数:code=400,detail=params error
	- 扩展id不正确:code=400,detail=extension_id error
	- 钱不够:code=403,detail=don"t have enough money
	- 成功:标准成功返回

### GET /api/download

功能:下载插件文件

如果插件的价格为0,未登录未购买也可下载(现在的情况)

参数:

id:下载的FileId ***注意不是扩展的ID,是文件的ID,这是两套不同的ID***

返回:

-   没有这个文件:code=404,detail=no file
-   没有购买或登录:code=403,detail=not sign or not buy