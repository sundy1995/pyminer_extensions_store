FROM python:3.8

WORKDIR /home/pyminer_extensions_store

COPY . .

RUN pip install -i https://pypi.tuna.tsinghua.edu.cn/simple  -r requirements.txt

EXPOSE 8000

CMD ["python", "store/manage.py", "test"]
