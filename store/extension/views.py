import os
from django.shortcuts import render
from django.http.response import JsonResponse, FileResponse
from django.core.files.base import ContentFile
from user.models import User
from django import forms
from django.views.generic import View
from store.settings import BASE_DIR, MEDIA_ROOT
from .models import Extension, File, ExtensionBill


# Create your views here.
def json(obj):
    try:
        code=obj['code']
        del obj['code']
    except:
        code=200
    response=JsonResponse(obj, safe=False)
    response.status_code=code
    return response


def get_user(request):
    try:
        return User.objects.get(id=request.session['user_id'])
    except:
        return None


def extension_list(request):
    result = [{
        'id': x.id,
        'name': x.name,
        'display_name': x.display_name,
        'author': x.author.username,
        'price': x.price
    } for x in Extension.objects.all()]
    return json(result)


def my_extension(request):
    user = get_user(request)
    if not user:
        return json({
            'code': 403,
            'detail': 'no sign in'
        })
    bills = user.bills.all()
    result = [
        {
            'id': bill.extension.id,
            'name': bill.extension.name,
            'display_name': bill.extension.display_name,
            'author': bill.extension.author.username
        } for bill in bills
    ]
    result += [
        {
            'id':extension.id,
            'name':extension.name,
            'display_name':extension.display_name,
            'author':user.username
        } for extension in user.made_extensions.all()
    ]
    return json(result)


class ExtensionView(View):
    def get_price(self,user_price):
        return 0
    def post(self,request):
        user = get_user(request)
        if not user:
            return json({
                'code': 403,
                'detail': 'no sign in'
            })
        try:
            name = request.POST['name']
            display_name = request.POST['display_name']
            price = self.get_price(request.POST.get('price', 0))
        except KeyError as e:
            return json({
                'code': 400,
                'detail': 'args wrong\n\n' + str(e)
            })
        else:
            if len(Extension.objects.filter(name=name)):
                return json({
                    'code': 409,
                    'detail': 'name must be unique'
                })
            Extension.objects.create(name=name, display_name=display_name,
                                    author=user, price=price)
            return json({
                'code': 200,
                'detail': 'success'
            })
    def get(self,request):
        if extension_id := request.GET.get('id'):
            try:
                extension = Extension.objects.get(id=extension_id)
            except:
                return json(
                    {"code": "404",
                     "detail": "不存在id为{}的插件".format(extension_id)})
        else:
            return
        return json({
            'id': extension.id,
            'name': extension.name,
            'display_name': extension.display_name,
            'author': extension.author.username,
            'price': extension.price
        })




def upload(request):
    user = get_user(request)
    if not user:
        return json({
            'code': 403,
            'detail': 'no sign in'
        })
    try:
        extension_id = request.POST['extension_id']
        version = request.POST['version']
        file_content = ContentFile(request.FILES.get('file').read())
    except KeyError as e:
        return json({
            'code': 400,
            'detail': 'params wrong'
        })
    try:
        extension = Extension.objects.get(id=extension_id)
        if extension.author.id != user.id:
            raise Exception()
    except:
        return json({
            'code': 400,
            'detail': 'extension_id is wrong'
        })
    if len(File.objects.filter(extension=extension,version=version))!=0:
        return json({
            'code':409,
            'detail':'file have been there'
        })
    file = File(extension=extension, version=version)
    file.file.save(extension.name + "_" + version, file_content)
    file.save()
    return json({
        'code': 200,
        'detail': 'success'
    })


def buy(request):
    user = get_user(request)
    if not user:
        return json({
            'code': 403,
            'detail': 'not sign in'
        })
    try:
        extension_id = request.POST['extension_id']
        extension = Extension.objects.get(id=extension_id)
    except KeyError as e:
        return json({
            'code': 400,
            'detail': 'params error'
        })
    except:
        return json({
            'code': 400,
            'detail': 'extension_id error'
        })
    if user.money < extension.price:
        return json({
            'code': 403,
            'detail': 'don\'t have enough money'
        })
    if ExtensionBill.objects.filter(user=user,extension=extension):
        return json({
            'code':400,
            'detail':'you have already bought it'
        })
    if user.made_extensions.filter(id=extension.id):
        return json({
            'code':403,
            'detail': 'the extension is made of you'
        })
    bill = ExtensionBill.objects.create(user=user, extension=extension)
    user.money -= extension.price
    user.save()
    return json({
        'code': 200,
        'detail': 'success'
    })


def download(request):
    try:
        file_id = request.GET['id']
        file = File.objects.get(id=file_id)
        extension = file.extension
    except KeyError:
        return json({
            'code': 400,
            'detail': 'param wrong'
        })
    except:
        return json({
            'code': 404,
            'detail': 'no file'
        })
    user = get_user(request)
    if extension.price == 0 or (
            user and extension.id in [bill.extension.id for bill in
                                      user.bills.all()]):
        path = os.path.join(BASE_DIR, MEDIA_ROOT, str(file.file))
        response = FileResponse(open(path, 'rb'))
        response['content-type'] = 'application/octet-stream'
        return response
    else:
        return json({
            'code': 403,
            'detail': 'not sign or not buy'
        })


def get_extension_version(request):
    if request.method == "GET":
        extension_id = request.GET.get("id", None)
        if extension_id is None:
            return json({
                "code": 403,
                "detail": "query fail! please check param!"
            })

        try:
            result = [{x.version: x.id} for x in
                      Extension.objects.filter(id=extension_id)[0].extensions.all()]
        except IndexError as e:
            return json({
                "code": 403,
                "detail": "query fail! please check param! {}".format(e)
            })

        return json(result)
