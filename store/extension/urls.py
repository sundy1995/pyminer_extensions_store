from django.urls import path
from . import views
urlpatterns=[
    path('extensions',views.extension_list),
    path('extension',views.ExtensionView.as_view()),
    path('extensions/my',views.my_extension),
    path('upload',views.upload),
    path('buy',views.buy),
    path('download',views.download),
    path('extensions_version', views.get_extension_version)
]