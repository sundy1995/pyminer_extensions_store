from django.test import TestCase
import json


class ExtensionTest(TestCase):
    def get_json_key(self, response: str, key: str = "code"):
        """
        从response中解析出参数key
        :param response: 服务器返回的json数据
        :param key: 需要解析的参数
        :return: 参数对应的value
        """
        return json.loads(response.decode("utf8"))[key]

    def regiser_login(self):
        """
        注册和登陆
        :return:
        """
        data = {"username": "john", "password": "123456"}
        self.client.post('/signup', data)

        # 登陆
        response = self.client.post("/signin", data)
        return response

    def test_extension_list(self):
        """
        测试获取插件列表
        :return:
        """
        self.regiser_login()
        # 插件信息
        data = {"name": "测试插件", "display_name": "测试插件", "price": 0}

        # 新建插件
        response = self.client.post("/new_extension", data=data)

        response = self.client.get("/extension_list")
        self.assertEqual(len(json.loads(response.content)) > 0, True,
                         "查询插件失败{}".format(response.content))

    def test_new_extension(self):
        """
        测试新建插件
        :return:
        """
        self.regiser_login()

        # 插件信息
        data = {"name": "测试插件", "display_name": "测试插件", "price": 0}

        response = self.client.post("/new_extension", data=data)
        self.assertEqual(self.get_json_key(response.content, "code"), 200,
                         "上传插件失败{}".format(response.content))

    def test_extension_info(self):
        """
        获取插件信息
        :return:
        """
        self.regiser_login()

        # 新建插件
        data = {"name": "测试插件", "display_name": "测试插件", "price": 0}
        self.client.post("/new_extension", data=data)

        # print("信息", self.client.get("/extension_info?id=1").content)
        response = self.client.get("/extension_info?id=1")
        self.assertEqual(len(json.loads(response.content)) > 1, True,
                         "获取插件信息失败{}".format(response.content))

    def test_buy(self):
        """
        测试购买插件
        :return:
        """
        # 登陆和注册
        self.regiser_login()

        # 新建插件
        data = {"name": "测试插件", "display_name": "测试插件", "price": 0}
        self.client.post("/new_extension", data=data)

        # 购买插件
        data = {"extension_id": 1}
        response = self.client.post("/buy", data=data)
        print(response.content)
        self.assertEqual(self.get_json_key(response.content, "code"), 200,
                         "购买插件失败{}".format(response.content))

    def test_my_extension(self):
        """
        测试 查询我购买的插件
        :return:
        """
        self.test_buy()

        response = self.client.get("/my_extension")

        self.assertEqual(len(json.loads(response.content)) > 0, True,
                         "查询我购买的插件失败{}".format(response.content))

    def test_upload(self):
        """
        为指定的插件上传一个新版本
        :return:
        """
        self.test_new_extension()

        with open("test_file.txt", "r", encoding="utf8") as file:
            data = {"extension_id": 1, "version": "2.1", "file": file}
            response = self.client.post("/upload", data=data)
            self.assertEqual(self.get_json_key(response.content, "code"), 200,
                             "上传新版本插件失败{}".format(response.content))

    def test_download(self):
        """
        测试下载插件
        :return:
        """
        # TODO 如何获取文件id？
        self.test_upload()
        data = {"id": 1}
        print(self.client.post("/download", data=data).content)

    def test_extensions_version(self):
        """

        :return:
        """
        # TODO
        self.test_upload()
        response = self.client.get("/extensions_version?id=1")
        print(response.content)
