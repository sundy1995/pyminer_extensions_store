from django.db import models
from user.models import User


# Create your models here.
class Extension(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, unique=True)
    display_name = models.CharField(max_length=50)
    author = models.ForeignKey(User, models.DO_NOTHING,
                               related_name='made_extensions')
    price = models.IntegerField(default=0)


class File(models.Model):
    id = models.AutoField(primary_key=True)
    extension = models.ForeignKey(Extension, models.DO_NOTHING,
                                  related_name='extensions')
    version = models.CharField(max_length=20)
    file = models.FileField()


class ExtensionBill(models.Model):
    id = models.AutoField(primary_key=True)
    extension = models.ForeignKey(Extension, models.DO_NOTHING,
                                  related_name='bills')
    user = models.ForeignKey(User, models.DO_NOTHING, related_name='bills')
