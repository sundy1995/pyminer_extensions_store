from django.test import TestCase
from user.models import User
import json


class UserTest(TestCase):

    def get_json_key(self, response: object, key: str = "code"):
        """
        从response中解析出参数key
        :param response: 服务器返回的json数据
        :param key: 需要解析的参数
        :return: 参数对应的value
        """
        return json.loads(response.decode("utf8"))[key]

    def set_up(self) -> object:
        """
        注册和登陆动作
        :return: 服务器返回的登陆后的json数据
        """
        data = {"username": "john", "password": "123456"}
        self.client.post('/signup', data)

        # 登陆
        response = self.client.post("/signin", data)
        return response

    def test_sign_up(self) -> None:
        """
        用户注册测试
        :return: None
        """
        data = {"username": "john", "password": "123456"}
        response = self.client.post('/signup', data)
        result = User.objects.filter(username="john")

        self.assertEqual(self.get_json_key(response.content, "code"), 200,
                         "用户注册失败，状态返回{}".format(response.content))
        self.assertEqual(len(result), 1, "用户注册失败，数据库中没有访问到")

    def test_sign_in(self) -> None:
        """
        用户登陆测试
        :return: None
        """
        response = self.set_up()
        self.assertEqual(self.get_json_key(response.content, key="code"), 200,
                         "用户登陆失败, {}".format(response.content))

    def test_signout(self) -> None:
        """
        测试退出登陆
        :return: Nine
        """
        response = self.set_up()

        # 退出
        response = self.client.get("/signout")

        self.assertEqual(self.get_json_key(response.content, key="code"), 200,
                         "退出登陆失败{}".format(response.content))

    def test_user_info(self) -> None:
        """
        测试获取用户信息页面
        :return: None
        """
        self.set_up()
        response = self.client.get("/user_info")
        self.assertEqual(self.get_json_key(response.content, "name"), "john",
                         "返回用户信息失败, {}".format(response.content))
