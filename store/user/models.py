from django.db import models


# Create your models here.
class User(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    username = models.CharField(max_length=20, unique=True)
    password = models.CharField(max_length=180)
    money = models.IntegerField(default=0)
