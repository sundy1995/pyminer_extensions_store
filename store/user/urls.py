from django.urls import path
from user import views
urlpatterns=[
    path('signin',views.sign_in),
    path('signout',views.sign_out),
    path('signup',views.sign_up),
    path('user_info',views.user_info)
]